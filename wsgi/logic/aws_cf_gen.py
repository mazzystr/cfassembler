#!/usr/bin/env python

"""
@author:  "Chris Callegari" <ccallega@redhat.com>
"""

import ConfigParser
import logging
import os
import sys
from logic.aws_inv import Aws_Inv    

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

IS_DEBUG = "--debug" in sys.argv

cf_header = { 
    "AWSTemplateFormatVersion":"2010-09-09",
    "Description":"" }
cf_params = {}
cf_params["SecurityGroups"] = {
    "Parameters":{
        "DefaultSg":{
            "Type":"String",
            "Description":"",
            "Default":"sg-7ec4da1b"
        },
        "ServiceName":{
            "Description":"",
            "Type":"String",
            "Default":"CRC"
        },
        "ServiceOwner":{
            "Description":"",
            "Type":"String",
            "Default":"CRC"
        },
        "ServicePhase":{
            "Description":"",
            "Type":"String",
            "Default":"Qa"
        },
        "SystemOperationsBucketName":{
            "Description":"",
            "Type":"String",
            "Default":"rh-system-operations"
        },
        "VpcId":{
            "Description":"",
            "Type":"String",
            "Default":"vpc-987762fa"
        },
        "VpcCidrRange":{
            "Description":"",
            "Type":"String",
            "Default":"10.3.96.0/20"
        } } }
cf_params["Systems"] = {}
cf_params["Shared"] = {}
cf_resources = {}
cf_outputs = {}
cf_outputs["SecurityGroups"] = {"Outputs": { } }


if IS_DEBUG:
    def debug():
        data = {}
        objects = {}
        data['Vpc'] = u'a1'
        data['tags'] = u'Nagios,Prod'
        objects['SecurityGroups'] = { "sg-abcd1234": {
                    "vpc_id": u"vpc-987762fa",
                    "name": u"VpcA1Utility-SecurityGroups-NagiosSg-1QZQ2RX3B60VP",
                    'rules': ['IPPermissions:tcp(443-443)', 'IPPermissions:tcp(80-80)'],
                    "tags": {
                        u"cumulus-stack": u"VpcA1Utility",
                        u"Name": u"NagiosSg",
                        u"ServicePhase": u"Prod",
                        u"aws:cloudformation:logical-id": u"NagiosSg",
                        u"ServiceComponent": u"Network",
                        u"aws:cloudformation:stack-name": u"VpcA1Utility-SecurityGroups",
                        u"ServiceName": u"Nagios",
                        u"ServiceOwner": u"Syseng" } } }
        thing = 'SecurityGroups'
        logger.info(data)
        logger.info(data['Vpc'])
        logger.info(data['tags'])
        logger.info(objects[thing])
        logger.info(thing)
        return data,objects,thing


class Aws_Cf_Gen:
    
    def __init__(self,data,objects):
        self.vpc_id=Aws_Inv().discover_vpc(data['Vpc'])
        self.newobjects=self.filter(data,objects,data['AWS'],self.vpc_id)
        self.merged=self.gen(data,objects,data['AWS'],self.newobjects)

    def find_aws_creds(self):
        if os.stat(os.environ['HOME'] + '/.boto'):
            botocreds = os.environ['HOME'] + '/.boto'
        else:
            botocreds = os.environ['HOME'] + '/app-root/runtime/repo/wsgi/creds/dev-cfassembler.boto'
        config = ConfigParser.ConfigParser()
        botocreds = config.read(botocreds)
        AWS_ACCESS_KEY_ID=config.get('Credentials', 'AWS_ACCESS_KEY_ID', 0)
        AWS_SECRET_ACCESS_KEY=config.get('Credentials', 'AWS_SECRET_ACCESS_KEY', 1)
        return AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY

    def filter(self,data,objects,thing,vpc_id):
        newobjects = []
        tags = data['tags'].split(',')
        for id in objects[thing]:
            score = 0
            if self.vpc_id == objects[thing][id]['vpc_id']:                
                for tag in tags:
                    try:
                        if tag == objects[thing][id]['tags']['ServiceName']:
                            score += 1
                    except:
                        pass
                    try:
                        if tag == objects[thing][id]['tags']['ServicePhase']:
                            score += 1
                    except:
                        pass
            if score == len(tags):
                try:
                    newobjects.append(id)
                except:
                    newobjects = [ id ]
        return newobjects

    def gen(self,data,objects,thing,newobjects):
        cf_resources["SecurityGroups"] = {"Resources": { } }
        merged = {}
        merged = {key: value for (key, value) in (cf_header.items() + cf_params[thing].items())}
        merged = {key: value for (key, value) in (merged.items() + cf_resources[thing].items())}
        for k in self.newobjects:
            repls = ('Dev',''), ('Qa',''), ('Stage',''), ('Prod','')
            objects[thing][k]['tags']['Name'] = reduce(lambda a, kv: a.replace(*kv), repls, objects[thing][k]['tags']['Name'])            
            obj = {"Type":"AWS::EC2::SecurityGroup",
                   "Properties":{
                       "GroupDescription":objects[thing][k]['tags']['Name'],
                       "SecurityGroupIngress":[],
                       "SecurityGroupEgress":[],
                       "Tags":[
                           {"Key":"Name","Value":objects[thing][k]['tags']['Name']},
                           {"Key":"ServiceName","Value":{"Ref":"ServiceName"}},
                           {"Key":"ServiceOwner","Value":{"Ref":"ServiceOwner"}},
                           {"Key":"ServicePhase","Value":{"Ref":"ServicePhase"}}],
                       "VpcId":{"Ref":"VpcId"}}}
            egress,ingress = Aws_Inv().discover_sg_rules(k)
            for e in egress:
                if egress[e]['ToPort'] != None or egress[e]['FromPort'] != None:
                    if 'sg-' in egress[e]['CidrIp']:
                        name,resource=self.gen_cf_sg_rule(objects[thing][k]['tags']['Name'],e,egress[e],'egress')
                        cf_resources[thing]['Resources'][name]=resource
                    else:
                        obj['Properties']['SecurityGroupEgress'].append(egress[e])
            for i in ingress:
                if ingress[i]['ToPort'] != None or ingress[i]['FromPort'] != None:
                    if 'sg-' in ingress[i]['CidrIp']:
                        name,resource=self.gen_cf_sg_rule(objects[thing][k]['tags']['Name'],i,ingress[i],'ingress')
                        cf_resources[thing]['Resources'][name]=resource
                    else:
                        obj['Properties']['SecurityGroupIngress'].append(ingress[i])
            cf_resources[thing]['Resources'][objects[thing][k]['tags']['Name']] = obj
        merged = {key: value for (key, value) in (merged.items() + cf_outputs[thing].items())}
        return merged

    def gen_cf_sg_rule(self,parent_name,num,rule,type):
        resource = {}
        name = parent_name.replace('Sg', type.title() + 'Rule' + str(num))
        if type == 'egress':
            SecurityGroupId = 'DestinationSecurityGroupId'
            Type = 'AWS::EC2::SecurityGroupEgress'
        if type == 'ingress':
            SecurityGroupId = 'SourceSecurityGroupId'
            Type = 'AWS::EC2::SecurityGroupIngress'
        target = rule['CidrIp'].split('-')
        target = target[0] + '-' + target[1]
        target = Aws_Inv().discover_sg(target)
        target = target.tags['Name']
        repls = ('Dev',''), ('Qa',''), ('Stage',''), ('Prod','')
        target = reduce(lambda a, kv: a.replace(*kv), repls, target)            
        resource = { "Type":Type,
                          "Properties":{
                              "IpProtocol":rule['IpProtocol'],
                              "FromPort":rule['FromPort'],
                              "ToPort":rule['ToPort'],
                              SecurityGroupId:{
                                  "Fn::GetAtt":[
                                      target,
                                      "GroupId"]},
                              "GroupId":{
                                  "Fn::GetAtt":[
                                      parent_name,
                                      "GroupId"]}}}
        return name,resource


if __name__ == '__main__':
    if IS_DEBUG:
        data,objects,thing = debug()
        Aws_Cf_Gen(data,objects,thing)