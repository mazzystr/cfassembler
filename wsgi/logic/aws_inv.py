#!/usr/bin/env python

"""
Tool for creating an inventory of AWS EC2 instances

@author:  "Chris Callegari" <ccallega@redhat.com>
"""

import ConfigParser
import argparse
import boto.ec2
import boto.iam
import logging
import os
import os.path
import sys
import textwrap
from boto.ec2 import connect_to_region
from boto.vpc import VPCConnection


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
    

class Aws_Inv:

    def __init__(self):
        self.regions = self.discover_regions()
        self.discover(self.regions)

    def find_aws_creds(self):
        if os.path.isfile(os.environ['HOME'] + '/.boto'):
            botocreds = os.environ['HOME'] + '/.boto'
        else:
            botocreds = os.environ['HOME'] + '/app-root/runtime/repo/wsgi/creds/dev-cfassembler.boto'
        config = ConfigParser.ConfigParser()
        botocreds = config.read(botocreds)
        AWS_ACCESS_KEY_ID=config.get('Credentials', 'AWS_ACCESS_KEY_ID', 0)
        AWS_SECRET_ACCESS_KEY=config.get('Credentials', 'AWS_SECRET_ACCESS_KEY', 1)
        return AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY

    def discover(self,regions):
        objects = {}
        try:
            AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=self.find_aws_creds()
        except:
            logger.info("Error: {0}: Cannot retrieve boto credentials".format('self.discover'))
            sys.exit()
        for r in regions:
            conn_ec2 = connect_to_region(r,
                                         aws_access_key_id=AWS_ACCESS_KEY_ID,
                                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            sgs = conn_ec2.get_all_security_groups()
            for sg in sgs:
                objects[sg.id] = {}
                #Add Egress/Ingress rules to objects.  We're already pulling
                # the data might as well pass it around and save on a later
                # API call
                #print 'Egress:', sg.rules_egress
                #print 'Ingress:', sg.rules
                try:
                    objects[sg.id]['vpc_id'] = sg.vpc_id
                except KeyError:
                    objects[sg.id]['vpc_id'] = 'Empty'
                try:
                    objects[sg.id]['name'] = sg.name
                except KeyError:
                    objects[sg.id]['name'] = 'Empty'  
                try:
                    objects[sg.id]['tags'] = sg.tags
                except KeyError:
                    objects[sg.id]['tags'] = {}                    
        return objects

    def discover_regions(self):
        # Add valid regions to this array when Red Hat allows those regions to
        # be used
        #valid_r = [ 'us-east-1', 'us-west-1', 'us-west-2' ]
        #if not self.args.region:
        #    regions = valid_r
        #else:
        #    regions = [ ]
        #    r = self.args.region.split(',')
        #    for i in r:
        #        if i in valid_r:
        #            regions.append(i)
        regions = ['us-east-1']
        return regions

    def discover_sg(self,sg):
        region='us-east-1'
        try:
            AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=self.find_aws_creds()
        except:
            logger.info("Error: {0}: Cannot retrieve boto credentials".format('self.discover_sg_rules'))

        conn_ec2 = connect_to_region(region,
                                     aws_access_key_id=AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
        sg = conn_ec2.get_all_security_groups(group_ids=sg)[0]
        return sg

    def discover_sg_rules(self,sg):
        region='us-east-1'
        egress = {}
        ingress = {}
        try:
            AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=self.find_aws_creds()
        except:
            logger.info("Error: {0}: Cannot retrieve boto credentials".format('self.discover_sg_rules'))

        conn_ec2 = connect_to_region(region,
                                     aws_access_key_id=AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
        rules = conn_ec2.get_all_security_groups(group_ids=sg)[0].rules
        i=0
        for j in range(len(rules)):
            for k in range(len(rules[j].grants)):
                ingress[i] = { 'IpProtocol': rules[j].ip_protocol,
                                'FromPort':rules[j].from_port,
                                'ToPort':rules[j].to_port,
                                'CidrIp': str(rules[j].grants[k]) }
                i += 1
        rules = conn_ec2.get_all_security_groups(group_ids=sg)[0].rules_egress
        i=0
        for j in range(len(rules)):
            for k in range(len(rules[j].grants)):
                egress[i] = { 'IpProtocol': rules[j].ip_protocol,
                                'FromPort':rules[j].from_port,
                                'ToPort':rules[j].to_port,
                                'CidrIp':str(rules[j].grants[k]) }
                i += 1
        return egress,ingress

    def discover_vpc(self,vpc_id):
        try:
            AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=self.find_aws_creds()
        except:
            logger.info("Error: {0}: Cannot retrieve boto credentials".format(__name__))
        try:
            conn_vpc = VPCConnection(aws_access_key_id=AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
            a = conn_vpc.get_all_vpcs()
            for b in a:
                if b.tags['Name'].lower() == vpc_id:
                    vpc_id=b.id
                else:
                    vpc_id=''
        except:
            logger.info("Error: {0}: conn_vpc cannot VPCConnection()".format(__name__))
        return vpc_id

#if __name__ == "__main__":
#    aws_inv()
