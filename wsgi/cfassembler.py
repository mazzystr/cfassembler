#!/usr/bin/python
import os
import json
import logging
import subprocess
import threading
import uuid
import sys

from flask import Flask, render_template, abort, jsonify, request
from threading import Thread

from logic.aws_cf_gen import Aws_Cf_Gen
from logic.aws_inv import Aws_Inv


os.environ["BOTO_CONFIG"] = "/Users/ccallega/Downloads/git/cfassembler/wsgi/creds/dev-cfassembler.boto"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

DEBUG = True
CFAssembler = Flask(__name__)

accounts = [ 'dev' ]
things = [ 'SecurityGroups' ]
vpcs = [ { "dev": "a1" } ]
objects = {}


# START PYTHON/FLASK WEB ROUTING
@CFAssembler.route('/', methods=['GET', 'POST'])
def index():
    regions = Aws_Inv().discover_regions()
    return render_template('index.html',
                           accounts=accounts,
                           regions=regions,
                           things=things,
                           vpcs=vpcs)

@CFAssembler.route('/gen/', methods=['GET', 'POST'])
def gen():
    data = request.args
    try:
        logger.info("Generating AWS CloudFormation: {0} : STARTED".format('blah'))
        a=Aws_Cf_Gen(data, objects)
        logger.info("Generating AWS CloudFormation: {0} : COMPLETE".format('blah'))
        jdata=json.dumps(a.merged, sort_keys=True, indent=4, separators=(',', ': '))
    except() as detail:
        logger.info("Generating AWS CloudFormation: {0} : FAILED".format('blah'))
        jdata = "Generating AWS CloudFormation: {0} : FAILED".format('blah')
        print detail
    return render_template('generated.html',
                           data=data,
                           jdata=jdata,
                           objects=objects,
                           things=things)

@CFAssembler.route('/inv', methods=['GET', 'POST'])
def inv():
    data = request.args
    regions = request.args.get('Region')
    thing = request.args.get('AWS')
    id = str(uuid.uuid4())
    try:
        logger.info("Inventory of AWS thing: {0} : STARTED".format(thing))
        objects[thing] = {}
        regions = Aws_Inv().discover_regions()
        objects[thing] = Aws_Inv().discover(regions)
        logger.info("Inventory of AWS thing: {0} : COMPLETE".format(thing))
    except subprocess.CalledProcessError:
        logger.info("Inventory of AWS thing: {0} : FAILED".format(thing))

    return render_template('inventory.html',
                           data=data,
                           id=id)

@CFAssembler.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


# START APPLICATION CODE
def app_start(things):
    logger.info('Initializing CFBuilder application')
    logger.info('Starting full inventory of AWS things')
    for thing in things:
        try:
            logger.info("Inventory of AWS thing: {0} : STARTED".format(thing))
            regions = Aws_Inv().discover_regions()
            objects[thing] = Aws_Inv().discover(regions)
            logger.info("Inventory of AWS thing: {0} : COMPLETE".format(thing))
        except:
            logger.info("Inventory of AWS thing: {0} : FAILED".format(thing))
            
def split_space(string):
    return string.strip().split()


# START PYTHON/FLASK APPLICATION
if __name__ == '__main__':
    
    try:
        wsgi=os.environ['OPENSHIFT_PYTHON_DIR']
    except:
        pass

    try:
        if wsgi:
            os.environ["BOTO_CONFIG"] =os.path.join(os.environ['HOME'], '/app-root/runtime/repo/wsgi/creds/dev-cfassembler.boto')
            virtenv = os.environ['OPENSHIFT_PYTHON_DIR'] + '/virtenv/'
            os.environ['PYTHON_EGG_CACHE'] = os.path.join(virtenv, 'lib/python2.7/site-packages')
            virtualenv = os.path.join(virtenv, 'bin/activate_this.py')
            try:
                execfile(virtualenv, dict(__file__=virtualenv))
            except IOError:
                pass
    except:
        os.environ["BOTO_CONFIG"] =os.path.join(os.environ['HOME'], 'Downloads/git/cfassembler/wsgi/creds/dev-cfassembler.boto')
        virtualenv = '/Users/ccallega/Downloads/git/cfassembler/virtenv'
        virtualenv = os.path.join(virtualenv, 'bin', 'activate_this.py')
        sys.path.append(virtualenv)
        try:
            execfile(virtualenv, dict(__file__=virtualenv))
        except IOError:
            pass
        threading.Thread(target=lambda: app_start(things)).start()
        CFAssembler.jinja_env.filters['split_space'] = split_space
        CFAssembler.run(host='127.0.0.1',port=8000)
