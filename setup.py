from setuptools import setup

setup(name='cfassembler',
      version='1.0',
      description='OpenShift App',
      author='Chris Callegari',
      author_email='ccallega@redhat.com',
      url='http://www.python.org/sigs/distutils-sig/',
      install_requires=[
      'Boto',
      'Flask',
      'Flask-FlatPages',
      'cStringIO',
      'json',
      'logging',
      'pyCurl',
      'textwrap' ],
     )
